package com.chawico.showcase.clean;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Responsible for the application initialization.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/09/2019
 */
@SpringBootApplication
public class BootCleanArchitectureApplication {
  
  /**
   * Main method
   * 
   * @param args {@code String[]} - initialization arguments.
   */
  public static void main(String... args) {
    SpringApplication.run(BootCleanArchitectureApplication.class, args);
  }
}
