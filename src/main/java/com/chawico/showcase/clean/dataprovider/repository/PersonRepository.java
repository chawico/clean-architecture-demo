package com.chawico.showcase.clean.dataprovider.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.chawico.showcase.clean.dataprovider.entity.PersonEntity;

/**
 * Repository that retrieves a Person from the database.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/09/2019
 */
@Repository
public interface PersonRepository extends CrudRepository<PersonEntity, Long> {

}
