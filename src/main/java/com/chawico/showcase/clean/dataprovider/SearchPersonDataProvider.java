package com.chawico.showcase.clean.dataprovider;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chawico.showcase.clean.dataprovider.entity.PersonEntity;
import com.chawico.showcase.clean.dataprovider.mapper.PersonDataProviderMapper;
import com.chawico.showcase.clean.dataprovider.repository.PersonRepository;
import com.chawico.showcase.clean.usecase.domain.PersonDomain;
import com.chawico.showcase.clean.usecase.gateway.SearchPersonGateway;

/**
 * Searches for a person in the database.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/09/2019
 */
@Service
public class SearchPersonDataProvider implements SearchPersonGateway {

  @Autowired
  private PersonRepository personRepository;
  
  @Override
  public Optional<PersonDomain> findPersonById(Long personId) {
    Optional<PersonEntity> person = this.personRepository.findById(personId);
    Optional<PersonDomain> personDomain = PersonDataProviderMapper.toDomain(person);
    
    return personDomain;
  }
}
