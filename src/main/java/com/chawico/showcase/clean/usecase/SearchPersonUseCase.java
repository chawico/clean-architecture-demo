package com.chawico.showcase.clean.usecase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.chawico.showcase.clean.usecase.domain.PersonDomain;
import com.chawico.showcase.clean.usecase.gateway.SearchPersonGateway;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * Use Case responsible for searching a Person.
 *
 * @author Charles Coco {@literal <charleswc@gmail.com>}
 * @since 09/09/2019
 */
@Component
public class SearchPersonUseCase {

  @Autowired
  private SearchPersonGateway searchPersonGateway;

  /**
   * Finds a person by its identifier.
   * 
   * @param personId {@code Long} - person identifier.
   * @return a person if found or empty if not.
   */
  public Optional<PersonDomain> findPersonById(Long personId) {
    
    if (personId == null || personId.compareTo(0L) < 0) {
      throw new IllegalArgumentException("No null or negative Person identifier value is acceptable!");
    }
    
    Optional<PersonDomain> optionalPerson = this.searchPersonGateway.findPersonById(personId);
    
    if (optionalPerson.isPresent()) {
      PersonDomain person = optionalPerson.get();
      person.setProfileCompleteness(this.calculateCompleteness(person));
    }

    return optionalPerson;
  }
  
  private BigDecimal calculateCompleteness(PersonDomain personDomain) {
    int filledFields = 0;
    int totalFields = 0;
    
    filledFields += (personDomain.getFullName() == null ? 0 : 1);
    totalFields++;

    filledFields += (personDomain.getEmailAddress() == null ? 0 : 1);    
    totalFields++;
    
    return BigDecimal.valueOf(filledFields).divide(BigDecimal.valueOf(totalFields));
  }
}
